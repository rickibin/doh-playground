//
//  WebViewController.swift
//  DoH
//
//  Created by Ricki Bin Yamin on 23/03/22.
//

import Network
import UIKit
import WebKit

final class WebViewController: UIViewController {
    
    let loadHTMLString: Bool
    
    private lazy var progressView: UIProgressView = {
        let progressView: UIProgressView = UIProgressView(progressViewStyle: .default)
        progressView.translatesAutoresizingMaskIntoConstraints = false
        
        return progressView
    }()
    
    private lazy var webView: WKWebView = {
        let webConfiguration = WKWebViewConfiguration()
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        return webView
    }()
    
    private var estimatedProgressObserver: NSKeyValueObservation?
    private var titleObserver: NSKeyValueObservation?
    private let urlString: String
    
    init(urlString: String, loadFromHTMLString: Bool) {
        self.urlString = urlString
        self.loadHTMLString = loadFromHTMLString
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        
        setupViews()
        configureObserver()
        configureWebview()
    }
    
    private func setupViews() {
        view.backgroundColor = .white
        view.addSubview(progressView)
        view.addSubview(webView)
        
        NSLayoutConstraint.activate([
            progressView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            progressView.leftAnchor.constraint(equalTo: view.leftAnchor),
            progressView.rightAnchor.constraint(equalTo: view.rightAnchor),
            progressView.heightAnchor.constraint(equalToConstant: 3),
            
            webView.topAnchor.constraint(equalTo: progressView.bottomAnchor),
            webView.leftAnchor.constraint(equalTo: view.leftAnchor),
            webView.rightAnchor.constraint(equalTo: view.rightAnchor),
            webView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func configureObserver() {
        estimatedProgressObserver = webView.observe(\.estimatedProgress, options: [.new]) { [weak self] webView, _ in
            self?.progressView.progress = Float(webView.estimatedProgress)
        }
        
        titleObserver = webView.observe(\.title, options: [.new]) { [weak self] webView, _ in
            self?.title = webView.title
        }
    }
    
    private func configureWebview() {
        webView.navigationDelegate = self
        
        let secureDNS: DoHConfigurarion = .google
        NWParameters.PrivacyContext.default.requireEncryptedNameResolution(
            true,
            fallbackResolver: .https(
                secureDNS.httpsURL,
                serverAddresses: secureDNS.serverAddresses
            )
        )
        
        if loadHTMLString {
            // load by using HTML String, but not recommended
            URLSession.shared.dataTask(with: URLRequest(url: URL(string: urlString)!)) { [weak self] data, response, error in
                let htmlString: String = String(data: data!, encoding: .ascii)!
                DispatchQueue.main.async {
                    self?.webView.loadHTMLString(htmlString, baseURL: nil)
                }
            }
            .resume()
        }
        else {
            // load webview as usual
            let request = URLRequest(url: URL(string: urlString)!)
            webView.load(request)
        }
    }
    
}

extension WebViewController: WKNavigationDelegate {
    func webView(_: WKWebView, didStartProvisionalNavigation _: WKNavigation!) {
        if progressView.isHidden {
            progressView.isHidden = false
        }
        
        UIView.animate(withDuration: 0.33,
                       animations: { [weak self] in
            self?.progressView.alpha = 1.0
        })
    }
    
    func webView(_: WKWebView, didFinish _: WKNavigation!) {
        UIView.animate(withDuration: 0.33,
                       animations: { [weak self] in
            self?.progressView.alpha = 0.0
        },
                       completion: { [weak self] isFinished in
            self?.progressView.isHidden = isFinished
        })
    }
    
}
