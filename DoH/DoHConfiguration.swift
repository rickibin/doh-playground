//
//  DoHConfiguration.swift
//  DoH
//
//  Created by Ricki Bin Yamin on 23/03/22.
//

import Foundation
import Network

enum DoHConfigurarion: Hashable {
    case cloudflare
    case google
    
    var httpsURL: URL {
        switch self {
        case .cloudflare:
            return URL(string: "https://cloudflare-dns.com/dns-query")!
        case .google:
            return URL(string: "https://dns.google/dns-query")!
        }
    }
    
    var serverAddresses: [NWEndpoint] {
        switch self {
        case .cloudflare:
            return [
                NWEndpoint.hostPort(host: "1.1.1.1", port: 443),
                NWEndpoint.hostPort(host: "1.0.0.1", port: 443),
                NWEndpoint.hostPort(host: "2606:4700:4700::1111", port: 443),
                NWEndpoint.hostPort(host: "2606:4700:4700::1001", port: 443)
            ]
        case .google:
            return [
                NWEndpoint.hostPort(host: "8.8.8.8", port: 443),
                NWEndpoint.hostPort(host: "8.8.4.4", port: 443)
            ]
        }
    }
}
