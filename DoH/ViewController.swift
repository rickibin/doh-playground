//
//  ViewController.swift
//  DoH
//
//  Created by Ricki Bin Yamin on 23/03/22.
//

import Alamofire
import Network
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textfield: UITextField!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let secureDNS: DoHConfigurarion = .google
        NWParameters.PrivacyContext.default.requireEncryptedNameResolution(
            true,
            fallbackResolver: .https(
                secureDNS.httpsURL,
                serverAddresses: secureDNS.serverAddresses
            )
        )
        
        // MARK: - URL Session Task
//        URLSession.shared.dataTask(with: URLRequest(url: URL(string: "https://reddit.com")!)) { data, response, error in
//            if let error = error {
//                print("Error: \(error)")
//            }
//            print("HEHE:\(data)")
//            print("HOHO:\(response)")
//        }
//        .resume()
        
        // MARK: - Alamofire
        let request = AF.request("https://reddit.com")
        request.response { response in
            print("Status Code: \(response.response?.statusCode)")
            print("Data: \(response.data)")
        }
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
//        let webViewVC: WebViewController = WebViewController(urlString: textfield.text!, loadFromHTMLString: false)
        let webViewVC: WebViewController = WebViewController(urlString: textfield.text!, loadFromHTMLString: true)
        navigationController?.pushViewController(webViewVC, animated: true)
    }
    
}
